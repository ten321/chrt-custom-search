;
var searchList = false;
if ( document.getElementById( 'search-results-template' ) !== null ) {
    var source = document.getElementById( 'search-results-template' ).innerHTML;
    var template = Handlebars.compile( source );
    var context = chrt_cse_info;

    var chrt_searchList = jQuery( 'div.search-results' ).first();

    searchList = template( context );

    chrt_searchList.append( searchList );
}

jQuery( function( $ ) {
    $( '#chrt-search-sort' ).on( 'change', function() {
        var url = window.location.href;
        var sort = $( this ).val();
        if ( url.indexOf( 'sort' ) <= 0 ) {
            url += '&sort=' + sort;
        } else {
            var re = new RegExp( /sort=([^\&]*)/ );
            url = url.replace( re, 'sort=' + sort );
        }

        window.location.href = url;
        return false;
    } );
} );