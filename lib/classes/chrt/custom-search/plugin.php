<?php
/**
 * Implements the main functionality of the CHRT Custom Search plugin
 *
 * @version 2019.1
 * @TODO get AJAX search results replacement working instead of having to reload the full page
 */

namespace {
	if ( ! defined( 'ABSPATH' ) ) {
		die( 'You do not have permission to access this file directly.' );
	}
}

namespace CHRT\Custom_Search {

	use CHRT_Front_Page;

	if ( ! class_exists( 'Plugin' ) ) {
		class Plugin {
			/**
			 * @var \CHRT\Custom_Search\Plugin $instance holds the single instance of this class
			 * @access private
			 */
			private static $instance;
			/**
			 * @var string $version holds the version number for the plugin
			 * @access public
			 */
			public static $version = '2019.1.3';
			/**
			 * @var string $plugin_path the root path to this plugin
			 * @access public
			 */
			public static $plugin_path = '';
			/**
			 * @var string $plugin_url the root URL to this plugin
			 * @access public
			 */
			public static $plugin_url = '';
			/**
			 * @var string|array|\stdClass the information returned by the API request
			 * @access public
			 */
			public $api_results = '';

			/**
			 * @var int $_total_items the total number of results returned by the API
			 * @access private
			 */
			private $_total_items = 0;
			/**
			 * @var int $_total_pages the total number of pages of results
			 * @access private
			 */
			private $_total_pages = 1;
			/**
			 * @var int $_items_per_page the number of results shown on each page of results
			 * @access private
			 */
			private $_items_per_page = 10;

			/**
			 * Creates the \CHRT\Custom_Search\Plugin object
			 *
			 * @access private
			 * @since  0.1
			 */
			private function __construct() {
				require_once( self::plugin_dir_path( 'lib/includes/search-api-key.php' ) );
				if ( ! defined( 'CHRT_CSE_API_KEY' ) ) {
					define( 'CHRT_CSE_API_KEY', $chrt_search_api_key );
				}
				if ( ! defined( 'CHRT_CSE_ID' ) ) {
					define( 'CHRT_CSE_ID', $chrt_search_cx );
				}

				add_action( 'template_redirect', array( $this, 'template_redirect' ), 100 );
				add_action( 'after_setup_theme', array( $this, 'after_setup_theme' ), 100 );
			}

			/**
			 * Returns the instance of this class.
			 *
			 * @access  public
			 * @return  \CHRT\Custom_Search\Plugin
			 * @since   0.1
			 */
			public static function instance() {
				if ( ! isset( self::$instance ) ) {
					$className      = __CLASS__;
					self::$instance = new $className;
				}

				return self::$instance;
			}

			/**
			 * Custom logging function that can be short-circuited
			 *
			 * @access public
			 * @return void
			 * @since  0.1
			 */
			public static function log( $message ) {
				if ( isset( $_GET['debug'] ) ) {
					printf( '<pre><code>%s</code></pre>', $message );
				}

				if ( ! defined( 'WP_DEBUG' ) || false === WP_DEBUG ) {
					return;
				}

				error_log( '[CHRT CSE Debug]: ' . $message );
			}

			/**
			 * Set the root path to this plugin
			 *
			 * @access public
			 * @return void
			 * @since  1.0
			 */
			public static function set_plugin_path() {
				self::$plugin_path = plugin_dir_path( dirname( dirname( dirname( dirname( __FILE__ ) ) ) ) );
			}

			/**
			 * Set the root URL to this plugin
			 *
			 * @access public
			 * @return void
			 * @since  1.0
			 */
			public static function set_plugin_url() {
				self::$plugin_url = plugin_dir_url( dirname( dirname( dirname( dirname( __FILE__ ) ) ) ) );
			}

			/**
			 * Returns an absolute path based on the relative path passed
			 *
			 * @param string $path the path relative to the root of this plugin
			 *
			 * @access public
			 * @return string the absolute path
			 * @since  1.0
			 */
			public static function plugin_dir_path( $path = '' ) {
				if ( empty( self::$plugin_path ) ) {
					self::set_plugin_path();
				}

				$rt = self::$plugin_path;

				if ( '/' === substr( $path, - 1 ) ) {
					$rt = untrailingslashit( $rt );
				}

				return $rt . $path;
			}

			/**
			 * Returns an absolute URL based on the relative path passed
			 *
			 * @param string $url the URL relative to the root of this plugin
			 *
			 * @access public
			 * @return string the absolute URL
			 * @since  1.0
			 */
			public static function plugin_dir_url( $url = '' ) {
				if ( empty( self::$plugin_url ) ) {
					self::set_plugin_url();
				}

				$rt = self::$plugin_url;

				if ( '/' === substr( $url, - 1 ) ) {
					$rt = untrailingslashit( $rt );
				}

				return $rt . $url;
			}

			/**
			 * Sets up the custom search results template
			 *
			 * @access public
			 * @return void
			 * @since  2019.1
			 */
			public function template_redirect() {
				if ( ! is_search() ) {
					return;
				}

				remove_action( 'genesis_loop', array( CHRT_Front_Page::instance(), 'do_search_results' ) );
				add_action( 'genesis_loop', array( $this, 'do_search_results' ) );

				add_filter( 'chrt-search-title', array( $this, 'search_title' ) );
				add_action( 'chrt-search-header', array( $this, 'search_header' ) );

				//remove_all_filters( 'get_search_form' );
			}

			/**
			 * Undo anything that we need to undo that was added to the after_setup_theme action
			 *
			 * @access public
			 * @return void
			 * @since  2019.1
			 */
			public function after_setup_theme() {
				global $chrt_theme_obj;

				remove_filter( 'get_search_form', array( $chrt_theme_obj, 'do_search_form' ), 99 );
			}

			/**
			 * Perform the API request
			 *
			 * @param string $url the URL for the API request
			 * @param array $args the arguments for the API request
			 *
			 * @access private
			 * @return void
			 * @since  2019.1
			 */
			private function _set_api_results( $url = '', $args = array() ) {
				if ( empty( $args ) ) {
					$args = $this->_get_cse_args();
				}
				if ( empty( $url ) ) {
					$url = $this->_get_cse_api_url();
				}

				$url = add_query_arg( $args, $url );
				self::log( 'Preparing to request ' . $url );

				$request = wp_remote_get( $url );
				if ( is_wp_error( $request ) ) {
					self::log( $request->get_error_message() );
					$results = $this->_get_empty_search_response( $request );
				} else {

					$results = @json_decode( wp_remote_retrieve_body( $request ) );

					if ( ! property_exists( $results, 'items' ) ) {
						self::log( print_r( $results, true ) );

						$results = $this->_get_empty_search_response();
					}

					$this->_total_items = $results->queries->request[0]->totalResults;

					$this->_items_per_page = $results->queries->request[0]->count;

					$this->_total_pages = ceil( $this->_total_items / $results->queries->request[0]->count );

					if ( 0 === $this->_total_items ) {
						$results = $this->_get_empty_search_response();
					}
				}

				$results->chrt_info = array(
					'url'  => $this->_get_cse_api_url(),
					'args' => $args,
				);

				$this->api_results = $results;
			}

			/**
			 * Retrieve and return the API results
			 *
			 * @param string $url the URL for the API request
			 * @param array $args the arguments for the API request
			 *
			 * @access private
			 * @return \stdClass the API results
			 * @since  2019.1
			 */
			private function _get_api_results( $url = '', $args = array() ) {
				if ( ! empty( $this->api_results ) ) {
					return $this->api_results;
				}

				$this->_set_api_results( $url, $args );

				return $this->api_results;
			}

			/**
			 * Filter the search results page title
			 *
			 * @param string $title the current title
			 *
			 * @access public
			 * @return string the updated title
			 * @since  2019.1
			 */
			public function search_title( $title ) {
				return $title;
			}

			/**
			 * Output any extra meta information in the search results header area
			 *
			 * @access public
			 * @return void
			 * @since  2019.1
			 */
			public function search_header() {
				$args = $this->_get_cse_args();
				$url  = $this->_get_cse_api_url();

				$results = $this->_get_api_results( $url, $args );

				$refinements = $results->context->facets;

				echo '<div class="search-results-header clearfix">';

				self::log( print_r( $refinements, true ) );

				echo '<ul class="search-refinements one-half first">';

				array_unshift( $refinements, array(
					(object) array(
						'label'         => '',
						'anchor'        => 'All',
						'label_with_op' => 'more:all'
					)
				) );

				foreach ( $refinements as $ref ) {
					$u     = home_url( '/' );
					$uargs = array(
						's' => $args['q'],
					);
					if ( array_key_exists( 'sort', $args ) ) {
						$uargs['sort'] = $args['sort'];
					}
					if ( ! empty( $ref[0]->label ) ) {
						$uargs['label'] = $ref[0]->label;
					}

					$current = ( $args['facetLabel'] == __( 'all', 'chrt' ) ) ? '' : $args['facetLabel'];
					if ( $current == $uargs['label'] ) {
						$selected = ' class="current-selection"';
					} else {
						$selected = ' class="not-selected"';
					}

					$u = add_query_arg( $uargs, $u );
					printf( '<li><a href="%1$s"' . $selected . '>%2$s</a></li>', $u, $ref[0]->anchor );
				}
				echo '</ul>';

				$sort = array_key_exists( 'sort', $args ) ? $args['sort'] : '';

				echo '<div class="search-sort one-half clearfix">';
				echo '<label for="chrt-search-sort">Sort by:</label>';
				echo '<span><select id="chrt-search-sort" name="sort">';
				echo '<option value=""' . selected( $_GET['sort'], '' ) . '>Relevance</option>';
				echo '<option value="date"' . selected( $_GET['sort'], 'date', false ) . '>Date</option>';
				echo '</select></span>';
				echo '</div>';

				echo '</div>';

				echo '<p class="entry-meta">';
				printf( __( 'About %d results' ), $this->_total_items );
				echo '</p>';
			}

			/**
			 * Output the search results
			 *
			 * @access public
			 * @return void
			 * @since  2019.1
			 */
			public function do_search_results() {
				$template = file_get_contents( self::plugin_dir_path( 'lib/includes/handlebars/templates/search-result.handlebars' ) );
				$args     = $this->_get_cse_args();
				$url      = $this->_get_cse_api_url();

				$results = $this->_get_api_results( $url, $args );

				$this->register_scripts();
				wp_enqueue_script( 'chrt-search-results' );

				$rt = '<script type="text/x-handlebars-template" id="search-results-template" data-page-number="1" data-total-pages="' . $this->_total_pages . '" data-total-items="' . $this->_total_items . '">';
				$rt .= $template;
				$rt .= '</script>';

				echo $rt;

				$rt = '<script type="text/javascript">';

				$rt .= 'var chrt_cse_info = ' . json_encode( $results ) . ';';
				$rt .= '</script>';

				echo $rt;

				echo '<div class="chrt-search">';

				echo '<div class="search-results"></div>';

				$this->_do_search_pagination( $args );

				echo '</div>';
			}

			/**
			 * Output the search results pagination area
			 *
			 * @param array $args the additional arguments for the URL
			 *
			 * @access private
			 * @return void
			 * @since  2019.1
			 */
			private function _do_search_pagination( $args = array() ) {
				echo '<footer class="archive-pagination pagination search-pagination"><ul>';

				if ( isset( $_GET['page'] ) && is_numeric( $_GET['page'] ) ) {
					$paged = $_GET['page'];
				} else {
					$paged = 1;
				}

				$base_url = \home_url( '/' );
				$args     = $_GET;

				self::log( print_r( $args, true ) );

				if ( array_key_exists( 'page', $args ) ) {
					unset( $args['page'] );
				}

				if ( $paged > 1 ) {
					$urlargs = array_merge( $args, array( 'page' => ( $paged - 1 ) ) );
					$url     = add_query_arg( $urlargs, $base_url );
					echo '<li class="pagination-previous"><a href="' . $url . '">' . __( 'Previous Page', 'chrt' ) . '</a></li>';
				}

				for ( $i = 1; $i <= $this->_total_pages; $i ++ ) {
					$urlargs = array_merge( $args, array( 'page' => $i ) );
					$url     = add_query_arg( $urlargs, $base_url );

					if ( $i === intval( $paged ) ) {
						$active = array(
							'class'        => 'active',
							'aria-label'   => __( 'Current Page', 'chrt' ),
							'aria-current' => 'page',
						);
					} else {
						$active = array(
							'class'        => '',
							'aria-label'   => '',
							'aria-current' => '',
						);
					}

					echo '<li' . ( empty( $active['class'] ) ? '' : ' class="' . $active['class'] . '"' ) . '>';
					echo '<a href="' . $url . '"' . ( empty( $active['aria-label'] ) ? '' : ' aria-label="' . $active['aria-label'] . '"' ) . ( empty( $active['aria-current'] ) ? '' : ' aria-current="' . $active['aria-current'] . '"' ) . '>';
					echo $i;
					echo '</a></li>';
				}

				if ( $paged < $this->_total_pages ) {
					$urlargs = array_merge( $args, array( 'page' => ( $paged + 1 ) ) );
					$url     = add_query_arg( $urlargs, $base_url );
					echo '<li class="pagination-next"><a href="' . $url . '">' . __( 'Next Page', 'chrt' ) . '</a></li>';
				}

				echo '</ul></footer>';
			}

			/**
			 * If Google doesn't return a list of refinements, we need to build our own list
			 *
			 * @access private
			 * @return array the list of refinements
			 * @since  2019.1
			 */
			private function _get_default_refinements() {
				$rt = array(
					array(
						(object) array(
							'label'         => 'publications',
							'anchor'        => __( 'Publications', 'chrt' ),
							'label_with_op' => 'more:publications'
						)
					),
					array(
						(object) array(
							'label'         => 'news',
							'anchor'        => __( 'News', 'chrt' ),
							'label_with_op' => 'more:news'
						)
					)
				);

				return $rt;
			}

			/**
			 * Register the necessary scripts for this plugin
			 *
			 * @access public
			 * @return void
			 * @since  2019.1
			 */
			public function register_scripts() {
				if ( ! wp_script_is( 'handlebars', 'registered' ) ) {
					wp_register_script( 'handlebars', self::_get_handlebars_url(), array(), '4.1.2', true );
				}
				if ( ! wp_script_is( 'chrt-search-results', 'registered' ) ) {
					wp_register_script( 'chrt-search-results', self::plugin_dir_url( '/lib/scripts/chrt/custom-search/search-results.js' ), array(
						'jquery',
						'handlebars'
					), self::$version, true );
				}

				if ( ! wp_style_is( 'chrt-search-results', 'enqueued' ) ) {
					wp_enqueue_style( 'chrt-search-results', self::plugin_dir_url( '/lib/styles/chrt/custom-search/style.css' ), array(), self::$version, 'all' );
				}
			}

			/**
			 * Return an empty list of search results when an error occurs
			 * @param null|\WP_Error the error, if there was one
			 *
			 * @access private
			 * @return \stdClass the empty result information
			 * @since  2019.1
			 */
			private function _get_empty_search_response( $error=null ) {
				$rt                   = new \stdClass();
				$rt->queries          = new \stdClass();
				$rt->queries->request = array(
					(object) array(
						'totalResults' => 1,
						'count'        => 1,
					),
				);
				$rt->items            = array(
					(object) array(
						'htmlTitle'    => __( 'No results found', 'chrt' ),
						'link'         => '#',
						'htmlSnippet'  => __( 'Unfortunately, no search results could be loaded at this time. Please try a different search or try again later.<!-- Error details: ' . print_r( $error, true ) . ' -->', 'chrt' ),
						'formattedUrl' => '',
					),
				);
				$rt->context          = (object) array(
					'facets' => $this->_get_default_refinements(),
				);

				$this->_total_pages = 1;
				$this->_total_items = 1;

				return $rt;
			}

			/**
			 * Retrieve the URL for the Google CSE JSON API
			 *
			 * @access private
			 * @return string the URL
			 * @since  2019.1
			 */
			private static function _get_cse_api_url() {
				return 'https://www.googleapis.com/customsearch/v1';
			}

			/**
			 * Retrieve the GET args for the search request
			 *
			 * @access private
			 * @return array an array of the arguments
			 * @since  2019.1
			 */
			private function _get_cse_args() {
				$args = array(
					'key' => CHRT_CSE_API_KEY,
					'cx'  => CHRT_CSE_ID,
				);

				if ( isset( $_GET['s'] ) ) {
					$args['q'] = urlencode( $_GET['s'] );
				}

				if ( isset( $_GET['sort'] ) ) {
					$args['sort'] = ( 'date' == strtolower( $_GET['sort'] ) ) ? 'date' : '';
				}

				if ( isset( $_GET['label'] ) ) {
					switch ( strtolower( $_GET['label'] ) ) {
						case 'publications' :
							$args['siteSearch'] = self::home_url( '/publication/' );
							$args['facetLabel'] = $_GET['label'];
							break;
						case 'news' :
							$args['siteSearch'] = self::home_url( '/news/' );
							$args['facetLabel'] = $_GET['label'];
							break;
						default :
							$args['siteSearch'] = self::home_url( '/' );
							$args['facetLabel'] = __( 'all', 'chrt' );
							break;
					}
				}

				$page = ( isset( $_GET['page'] ) ? $_GET['page'] : 1 );
				if ( $page > 1 ) {
					$args['start'] = ( ( ( $page - 1 ) * $this->_items_per_page ) + 1 );
				}

				return $args;
			}

			/**
			 * Retrieve and return the appropriate home URL
			 *
			 * @param string $url the path being added to the home URL
			 *
			 * @access public
			 * @return string
			 * @since  2019.1
			 */
			public static function home_url( $url ) {
				$d = $_SERVER['HTTP_HOST'];
				self::log( 'Current host is: ' . print_r( $d, true ) );

				if ( 'chrt.org' == $d || 'www.chrt.org' == $d ) {
					return str_replace( array( 'https://', 'http://' ), '', home_url( $url ) );
				}

				return str_replace( array( 'https://', 'http://', $d ), array( '', '', 'chrt.org' ), home_url( $url ) );
			}

			/**
			 * Retrieve and return the location of the Handlebars JS
			 *
			 * @access private
			 * @return string the URL to the Handlebars JS
			 * @since  0.1
			 */
			private static function _get_handlebars_url() {
				$min = '.min';
				if ( defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ) {
					$min = '';
				}

				return 'https://cdnjs.cloudflare.com/ajax/libs/handlebars.js/4.1.2/handlebars' . $min . '.js';
			}
		}
	}
}