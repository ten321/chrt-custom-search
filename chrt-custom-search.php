<?php
/*
Plugin Name: CHRT Custom Search
Description: Implements a Google Custom Search using the CSE JSON API
Version: 2019.1.2
Author: Curtiss Grymala
Author URI: http://ten-321.com/
License: GPL2
*/

namespace {
	spl_autoload_register( function ( $class_name ) {
		if ( ! stristr( $class_name, 'CHRT\Custom_Search\\' ) ) {
			return;
		}

		$filename = plugin_dir_path( __FILE__ ) . '/lib/classes/' . strtolower( str_replace( array(
				'\\',
				'_'
			), array( '/', '-' ), $class_name ) ) . '.php';

		if ( ! file_exists( $filename ) ) {
			return;
		}

		include $filename;
	} );
}

namespace CHRT\Custom_Search {
	if ( ! isset( $hsu_transfer_obj ) || ! is_a( $hsu_transfer_obj, '\CHRT\Custom_Search\Plugin' ) ) {
		$GLOBALS['chrt_search_object'] = \CHRT\Custom_Search\Plugin::instance();
	}
}